#!/bin/python3
import sys
curr_date = None
curr_count = 0
for line in sys.stdin:
    date, count = line.split('\t')
    count = int(count)
    if date == curr_date:
        curr_count += count
    else:
        if curr_date:
            print(f"{curr_date}\t{curr_count}")
        curr_date = date
        curr_count = count

if curr_date == date:
    print(f"{curr_date}\t{curr_count}")