#!/bin/python3
import sys
import datetime
for line in sys.stdin:
    date, time, direction, speed, access = line.split(",")
    if direction == '"Acercamiento"':
        date = datetime.datetime.fromisoformat(eval(date))
        access = access.replace("\n", "")
        print(f"{date.month}\t{access}\t1")