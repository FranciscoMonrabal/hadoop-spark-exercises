#!/bin/python3
import sys
curr_month = None
counters = {}

def increment_counter(counters, count, access):
    if access in counters:
        counters[access] += count
    else:
        counters[access] = count

for line in sys.stdin:
    date, access, count = line.split('\t')
    count = int(count)
    if date == curr_month:
        increment_counter(counters, count, access)
    else:
        if curr_month:
            print(f"{curr_month}\t{counters}")
        curr_month = date
        counters = {}
        increment_counter(counters, count, access)

if curr_month == date:
    print(f"{curr_month}\t{counters}")

