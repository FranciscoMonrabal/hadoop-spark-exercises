#!/bin/python3
import sys
curr_date = None
counters = {}
curr_day = 0

'''
current_day values:
    Monday:     0
    Tuesday:    1
    Wednesday:  2
    Thursday:   3
    Friday:     4
    Saturday:   5
    Sunday:     6
'''

def increment_counter(counters, count, curr_day):
    if curr_day in counters:
        counters[curr_day] += count
    else:
        counters[curr_day] = count

for line in sys.stdin:
    date, count = line.split('\t')
    count = int(count)
    if date == curr_date:
        increment_counter(counters, count, curr_day)
    else:
        if curr_day < 6:
            curr_day = curr_day + 1
        else:
            curr_day = 0

        curr_date = date
        increment_counter(counters, count, curr_day)

if curr_date == date:

    print(counters)
    total_days = int(365/7) # Yes this is far from perfect, because not all week days has the same quantity of days

    for day,total_cars in counters.items():
        mean = total_cars/total_days
        print(f"{day}\tMean: {mean}")