#!/bin/python3
import sys
import datetime
for line in sys.stdin:
    date, time, direction, speed, access = line.split(",")
    if direction == '"Acercamiento"':
        date = datetime.datetime.fromisoformat(eval(date))
        speed = speed.replace('"','')
        print(f"{date.year}-{date.month}-{date.day}\t{date.hour}\t{speed}\t1")