#!/bin/python3
import sys
curr_date = None
counters = {}
curr_day = 0

'''
current_day values:
    Monday:     0
    Tuesday:    1
    Wednesday:  2
    Thursday:   3
    Friday:     4
    Saturday:   5
    Sunday:     6
'''

def increment_counter(counters, count, curr_day, curr_hour, speed):
    if curr_day in counters:
        if curr_hour in counters[curr_day]:
            counters[curr_day][curr_hour]["cars"] += count
            counters[curr_day][curr_hour]["speed"] += speed
        else:
            counters[curr_day][curr_hour] = {}
            counters[curr_day][curr_hour]["cars"] = count
            counters[curr_day][curr_hour]["speed"] = speed
    else:
        counters[curr_day] = {}
        counters[curr_day][curr_hour] = {}
        counters[curr_day][curr_hour]["cars"] = count
        counters[curr_day][curr_hour]["speed"] = speed

for line in sys.stdin:
    date, hour, speed, count = line.split('\t')
    count = int(count)
    speed = int(speed)
    if date == curr_date:
        increment_counter(counters, count, curr_day, hour, speed)
    else:
        if curr_day < 6:
            curr_day = curr_day + 1
        else:
            curr_day = 0

        curr_date = date
        increment_counter(counters, count, curr_day, hour, speed)

if curr_date == date:

    print(counters)

    for day, hours in counters.items():
        for hour, info in hours.items():
            mean = info["speed"]/info["cars"]
            print(f"{day}\t{hour}\tMean: {mean}")