import datetime
import pyspark

 # YARN

'''
sc = pyspark.SparkContext()
(sc.textFile("/user/alumno/lab/input/datos.csv")
    .filter(lambda x: x.split(",")[2] == '"Acercamiento"')
    .map(lambda x: (f'{datetime.datetime.fromisoformat(eval(x.split(",")[0])).month}\t{eval(x.split(",")[4])}', 1))
    .reduceByKey(lambda a,b: a+b )
    .sortByKey()
    .saveAsTextFile("/user/alumno/lab/output/resultados1.csv"))
'''

# Local

sc = pyspark.SparkContext()
(sc.textFile("file:///home/alumno/spark-3.2.0-bin-hadoop3.2/dev/Ejercicio3/datos.csv")
    .filter(lambda x: x.split(",")[2] == '"Acercamiento"')
    .map(lambda x: (f'{datetime.datetime.fromisoformat(eval(x.split(",")[0])).month}\t{eval(x.split(",")[4])}', 1))
    .reduceByKey(lambda a,b: a+b )
    .sortByKey()
    .saveAsTextFile("file:///home/alumno/spark-3.2.0-bin-hadoop3.2/dev/Ejercicio3/resultados"))