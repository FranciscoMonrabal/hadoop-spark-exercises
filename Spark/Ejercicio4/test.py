import datetime
import pyspark

 # YARN

'''
sc = pyspark.SparkContext()
(sc.textFile("/user/alumno/lab/input/datos.csv")
    .filter(lambda x: x.split(",")[2] == '"Acercamiento"')
    .map(lambda x: (f'{datetime.datetime.fromisoformat(eval(x.split(",")[0])).weekday()}', 1))
    .reduceByKey(lambda a,b: a+b )
    .sortByKey()
    .map(lambda x: (x[0], x[1]/52))
    .saveAsTextFile("/user/alumno/lab/output/resultados1.csv"))
'''

# Local

sc = pyspark.SparkContext()
(sc.textFile("file:///home/alumno/spark-3.2.0-bin-hadoop3.2/dev/Ejercicio4/datos.csv")
    .filter(lambda x: x.split(",")[2] == '"Acercamiento"')
    .map(lambda x: (f'{datetime.datetime.fromisoformat(eval(x.split(",")[0])).weekday()}', 1))
    .reduceByKey(lambda a,b: a+b )
    .sortByKey()
    .map(lambda x: (x[0], x[1]/52))
    .saveAsTextFile("file:///home/alumno/spark-3.2.0-bin-hadoop3.2/dev/Ejercicio4/resultados"))